# Trabalhando com o Android Studio e Principais Ferramentas

## Ementa



## 🏆 Certifique seu Conhecimento

### Em Relação a configurações do JDK, é correto afirmar : 
* [x] - Propriedade do Sistema -> Variáveis de Ambiente -> Variáveis do Sistema-> Novo (nome recomendado: JAVA_HOME, valor: caminho do JDK ou JRE) - >Ok -> Path - > Editar->Novo e escrever ->%JAVA_HOME%\bin -> Ok 

### É correto afirmar sobre o SDK Platform-Tools de android Studio :
* [x] - O SDK Plataform-Tools é um componente do SDK do Android

### Sobre os Emuladores do Android Studio :  
* [x] - É possível executar o app de um projeto do Android Studio ou um app instalador no Android Emulador da mesma forma que você executa qualquer app em um dispositivo

### Sobre o Android Studio, é correto afirmar : 
* [x] - O Android Studio é um ambiemte de desenvolvimento oficial para desenvolvimento de apps android.

### Considerando o ambiemte de desenvolvimento Android Studio, assinale a alternativa que apresenta apenas linguagens válidas ao adicionar uma nova activity ao projeto.
* [x] - Java / Kotlin

### É Correto afirmar sobre o arquivo colors.xml de um projeto Android :
* [x] - Fornece métodos para criar, converter, manipular cores. Pode criar cores e usar no Projeto

### É correto afirmar sobre o arquivo AndroidManifest.xml de um projeto Android : 
* [x] - Todo projeto de aplicativo precisa ter um arquivo AndroidManifest.xml. O arquivo de manifesto descreve informações essenciais sobre o aplicativo.

### É correto afirmar sobre o arquivo string.xml de um projeto android : 
* [x] - Cada String é unica e pode ser referenciada do aplicativo ou de outros arquivos de recurso ( como um layout XML )

### Em Relação a executar um aplicativo android é correto afirmar : 
* [x] - Podem ser executados tanto no aparelho físico quanto no emulador do android Studio

### Em Relação a criação de um emulador no android Studio, é correto afirmar : 
* [x] - O AVD Manager é uma interface que pode ser iniciada no Android Studio para ajudar a criar e gerenciar um dispositivo virtual Android( Emulador)
